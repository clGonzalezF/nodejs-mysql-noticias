const dbConnection = require('../../config/dbConnection');
const dateFormat = require('dateformat');

module.exports = app => {
    const connection = dbConnection();

    app.get('/', (req, res) => {
        connection.query('SELECT * FROM news', (error, result) =>{
            res.render('news/news', {
                data: result,
                dateFormat: dateFormat
            });
        });
        //res.send('hola mundo');
    });

    app.post('/news', (req, res) => {
        console.log(req.body);
        const { title, content } = req.body;
        connection.query('INSERT INTO news SET?', {
            title: title,
            content: content
        }, (error, result) => {
            res.redirect('/');
        });
    });
}